diff --git a/src/Data/Singletons/TH/Options.hs b/src/Data/Singletons/TH/Options.hs
index 8a04b8b..2a78a31 100644
--- a/src/Data/Singletons/TH/Options.hs
+++ b/src/Data/Singletons/TH/Options.hs
@@ -1,3 +1,5 @@
+{-# LANGUAGE TemplateHaskellQuotes #-}
+
 -----------------------------------------------------------------------------
 -- |
 -- Module      :  Data.Singletons.TH.Options
@@ -283,6 +285,10 @@ singDataConName nm
 singTyConName :: Name -> Name
 singTyConName name
   | name == listName                                 = mkName "SList"
+    -- On GHC 9.8+, ''() is sugar for ''Unit, so tupleNameDegree_maybe won't
+    -- detect that it's in fact a tuple type. We add an explicit case here to
+    -- ensure that it continues to receive special naming treatment.
+  | name == ''()                                     = mkTupleName 0
   | Just degree <- tupleNameDegree_maybe name        = mkTupleName degree
     -- See Note [Promoting and singling unboxed tuples]
   | Just degree <- unboxedTupleNameDegree_maybe name = mkTupleName degree
diff --git a/src/Data/Singletons/TH/Promote.hs b/src/Data/Singletons/TH/Promote.hs
index ac120ba..2cd3200 100644
--- a/src/Data/Singletons/TH/Promote.hs
+++ b/src/Data/Singletons/TH/Promote.hs
@@ -1,3 +1,5 @@
+{-# LANGUAGE CPP #-}
+
 {- Data/Singletons/TH/Promote.hs
 
 (c) Richard Eisenberg 2013
@@ -160,7 +162,7 @@ promoteInstance :: OptionsMonad q => DerivDesc q -> String -> Name -> q [Dec]
 promoteInstance mk_inst class_name name = do
   (tvbs, cons) <- getDataD ("I cannot make an instance of " ++ class_name
                             ++ " for it.") name
-  tvbs' <- mapM dsTvbUnit tvbs
+  tvbs' <- mapM dsTvbVis tvbs
   let data_ty   = foldTypeTvbs (DConT name) tvbs'
   cons' <- concatMapM (dsCon tvbs' data_ty) cons
   let data_decl = DataDecl name tvbs' cons'
@@ -336,7 +338,7 @@ promoteClassDec decl@(ClassDecl { cd_name = cls_name
       -- /don't/ use the type variable binders from the method's type...
       (_, argKs, resK) <- promoteUnraveled ty
       args <- mapM (const $ qNewName "arg") argKs
-      let proTvbs = zipWith (`DKindedTV` ()) args argKs
+      let proTvbs = zipWith (`DKindedTV` bndrReq) args argKs
       -- ...instead, compute the type variable binders in a left-to-right order,
       -- since that is the same order that the promoted method's kind will use.
       -- See Note [Promoted class methods and kind variable ordering]
@@ -399,7 +401,7 @@ decided not to use this hack unless someone specifically requests it.
 -- returns (unpromoted method name, ALetDecRHS) pairs
 promoteInstanceDec :: OMap Name DType
                       -- Class method type signatures
-                   -> Map Name [DTyVarBndrUnit]
+                   -> Map Name [DTyVarBndrVis]
                       -- Class header type variable (e.g., if `class C a b` is
                       -- quoted, then this will have an entry for {C |-> [a, b]})
                    -> UInstDecl -> PrM AInstDecl
@@ -423,7 +425,7 @@ promoteInstanceDec orig_meth_sigs cls_tvbs_map
                                               inst_kis) meths']
     return (decl { id_meths = zip (map fst meths) ann_rhss })
   where
-    lookup_cls_tvbs :: PrM [DTyVarBndrUnit]
+    lookup_cls_tvbs :: PrM [DTyVarBndrVis]
     lookup_cls_tvbs =
       -- First, try consulting the map of class names to their type variables.
       -- It is important to do this first to ensure that we consider locally
@@ -435,7 +437,7 @@ promoteInstanceDec orig_meth_sigs cls_tvbs_map
           -- If the class isn't present in this map, we try reifying the class
           -- as a last resort.
 
-    reify_cls_tvbs :: PrM [DTyVarBndrUnit]
+    reify_cls_tvbs :: PrM [DTyVarBndrVis]
     reify_cls_tvbs = do
       opts <- getOptions
       let pClsName = promotedClassName opts cls_name
@@ -447,7 +449,7 @@ promoteInstanceDec orig_meth_sigs cls_tvbs_map
         Just tvbs -> pure tvbs
         Nothing -> fail $ "Cannot find class declaration annotation for " ++ show cls_name
 
-    extract_tvbs :: PrM (Maybe DInfo) -> MaybeT PrM [DTyVarBndrUnit]
+    extract_tvbs :: PrM (Maybe DInfo) -> MaybeT PrM [DTyVarBndrVis]
     extract_tvbs reify_info = do
       mb_info <- lift reify_info
       case mb_info of
@@ -559,7 +561,10 @@ promoteMethod meth_sort orig_sigs_map (meth_name, meth_rhs) = do
               -- We have an InstanceSig. These are easy: we can just use the
               -- instance signature's type directly, and no substitution for
               -- class variables is required.
-              promoteUnraveled ty
+              (_, arg_kis, res_ki) <- promoteUnraveled ty
+              let tvbs = changeDTVFlags SpecifiedSpec $
+                         toposortTyVarsOf (arg_kis ++ [res_ki])
+              pure (tvbs, arg_kis, res_ki)
             Nothing -> do
               -- We don't have an InstanceSig, so we must compute the kind to use
               -- ourselves.
@@ -584,7 +589,10 @@ promoteMethod meth_sort orig_sigs_map (meth_name, meth_rhs) = do
       case OMap.lookup meth_name orig_sigs_map of
         Just ty -> do
           -- The type of the method is in scope, so promote that.
-          promoteUnraveled ty
+          (_, arg_kis, res_ki) <- promoteUnraveled ty
+          let tvbs = changeDTVFlags SpecifiedSpec $
+                     toposortTyVarsOf (arg_kis ++ [res_ki])
+          pure (tvbs, arg_kis, res_ki)
         Nothing -> do
           -- If the type of the method is not in scope, the only other option
           -- is to try reifying the promoted method name.
@@ -801,10 +809,10 @@ promoteLetDecRHS rhs_sort type_env fix_env mb_let_uniq name let_dec_rhs = do
       opts <- getOptions
       tyvarNames <- replicateM ty_num_args (qNewName "a")
       let proName    = promotedValueName opts name mb_let_uniq
-          local_tvbs = map (`DPlainTV` ()) all_locals
+          local_tvbs = map (`DPlainTV` bndrReq) all_locals
           m_fixity   = OMap.lookup name fix_env
 
-          mk_tf_head :: [DTyVarBndrUnit] -> DFamilyResultSig -> DTypeFamilyHead
+          mk_tf_head :: [DTyVarBndrVis] -> DFamilyResultSig -> DTypeFamilyHead
           mk_tf_head tvbs res_sig = DTypeFamilyHead proName tvbs res_sig Nothing
 
           (lde_kvs_to_bind, m_sak_dec, defun_ki, tf_head) =
@@ -812,7 +820,7 @@ promoteLetDecRHS rhs_sort type_env fix_env mb_let_uniq name let_dec_rhs = do
             case m_ldrki of
               -- 1. We have no kind information whatsoever.
               Nothing ->
-                let all_args = local_tvbs ++ map (`DPlainTV` ()) tyvarNames in
+                let all_args = local_tvbs ++ map (`DPlainTV` bndrReq) tyvarNames in
                 ( OSet.empty
                 , Nothing
                 , DefunNoSAK all_args Nothing
@@ -820,7 +828,13 @@ promoteLetDecRHS rhs_sort type_env fix_env mb_let_uniq name let_dec_rhs = do
                 )
               -- 2. We have some kind information in the form of a LetDecRHSKindInfo.
               Just (LDRKI m_sak tvbs argKs resK) ->
-                let all_args         = local_tvbs ++ zipWith (`DKindedTV` ()) tyvarNames argKs
+                let
+#if __GLASGOW_HASKELL__ >= 909
+                    tvbs'            = mapMaybe kindVarToBndrVis tvbs
+#else
+                    tvbs'            = []
+#endif
+                    all_args         = tvbs' ++ local_tvbs ++ zipWith (`DKindedTV` bndrReq) tyvarNames argKs
                     lde_kvs_to_bind' = OSet.fromList (map extractTvbName tvbs) in
                 case m_sak of
                   -- 2(a). We do not have a standalone kind signature.
@@ -872,13 +886,7 @@ promoteLetDecRHS rhs_sort type_env fix_env mb_let_uniq name let_dec_rhs = do
     promote_let_dec_ty all_locals default_num_args =
       case rhs_sort of
         ClassMethodRHS tvbs arg_kis res_ki
-          -> -- For class method RHS helper functions, don't bother quantifying
-             -- any type variables in their SAKS. We could certainly try, but
-             -- given that these functions are only used internally, there's no
-             -- point in trying to get the order of type variables correct,
-             -- since we don't apply these functions with visible kind
-             -- applications.
-             let sak = ravelVanillaDType [] [] arg_kis res_ki in
+          -> let sak = ravelVanillaDType tvbs [] arg_kis res_ki in
              return (Just (LDRKI (Just sak) tvbs arg_kis res_ki), length arg_kis)
         LetBindingRHS
           |  Just ty <- OMap.lookup name type_env
@@ -1099,7 +1107,7 @@ promoteExp (DLamE names exp) = do
   (rhs, ann_exp) <- lambdaBind var_proms $ promoteExp exp
   all_locals <- allLocals
   let all_args = all_locals ++ tyNames
-      tvbs     = map (`DPlainTV` ()) all_args
+      tvbs     = map (`DPlainTV` bndrReq) all_args
   emitDecs [DClosedTypeFamilyD (DTypeFamilyHead
                                  lambdaName
                                  tvbs
@@ -1121,7 +1129,7 @@ promoteExp (DCaseE exp matches) = do
   (eqns, ann_matches) <- mapAndUnzipM (promoteMatch prom_case) matches
   tyvarName  <- qNewName "t"
   let all_args = all_locals ++ [tyvarName]
-      tvbs     = map (`DPlainTV` ()) all_args
+      tvbs     = map (`DPlainTV` bndrReq) all_args
   emitDecs [DClosedTypeFamilyD (DTypeFamilyHead caseTFName tvbs DNoSig Nothing) eqns]
     -- See Note [Annotate case return type] in Single
   let applied_case = prom_case `DAppT` exp'
diff --git a/src/Data/Singletons/TH/Promote/Defun.hs b/src/Data/Singletons/TH/Promote/Defun.hs
index 6d1b3a0..6d746fc 100644
--- a/src/Data/Singletons/TH/Promote/Defun.hs
+++ b/src/Data/Singletons/TH/Promote/Defun.hs
@@ -1,3 +1,4 @@
+{-# LANGUAGE CPP #-}
 {-# LANGUAGE TemplateHaskellQuotes #-}
 
 {- Data/Singletons/TH/Promote/Defun.hs
@@ -53,7 +54,7 @@ defunTopLevelTypeDecls ty_syns c_tyfams o_tyfams = do
 
 -- Defunctionalize all the type families associated with a type class.
 defunAssociatedTypeFamilies ::
-     [DTyVarBndrUnit]     -- The type variables bound by the parent class
+     [DTyVarBndrVis]      -- The type variables bound by the parent class
   -> [OpenTypeFamilyDecl] -- The type families associated with the parent class
   -> PrM ()
 defunAssociatedTypeFamilies cls_tvbs atfs = do
@@ -85,11 +86,11 @@ defunAssociatedTypeFamilies cls_tvbs atfs = do
     --
     -- Here, we know that `T :: Bool -> Type` because we can infer that the `a`
     -- in `type T a` should be of kind `Bool` from the class SAK.
-    ascribe_tf_tvb_kind :: DTyVarBndrUnit -> DTyVarBndrUnit
+    ascribe_tf_tvb_kind :: DTyVarBndrVis -> DTyVarBndrVis
     ascribe_tf_tvb_kind tvb =
       case tvb of
         DKindedTV{}  -> tvb
-        DPlainTV n _ -> maybe tvb (DKindedTV n ()) $ Map.lookup n cls_tvb_kind_map
+        DPlainTV n _ -> maybe tvb (DKindedTV n bndrReq) $ Map.lookup n cls_tvb_kind_map
 
 buildDefunSyms :: DDec -> PrM [DDec]
 buildDefunSyms dec =
@@ -121,7 +122,7 @@ buildDefunSymsOpenTypeFamilyD =
   buildDefunSymsTypeFamilyHead defaultTvbToTypeKind (Just . defaultMaybeToTypeKind)
 
 buildDefunSymsTypeFamilyHead
-  :: (DTyVarBndrUnit -> DTyVarBndrUnit) -- How to default each type variable binder
+  :: (DTyVarBndrVis -> DTyVarBndrVis)   -- How to default each type variable binder
   -> (Maybe DKind -> Maybe DKind)       -- How to default the result kind
   -> DTypeFamilyHead -> PrM [DDec]
 buildDefunSymsTypeFamilyHead default_tvb default_kind
@@ -130,7 +131,7 @@ buildDefunSymsTypeFamilyHead default_tvb default_kind
       res_kind = default_kind (resultSigToMaybeKind result_sig)
   defunReify name arg_tvbs res_kind
 
-buildDefunSymsTySynD :: Name -> [DTyVarBndrUnit] -> DType -> PrM [DDec]
+buildDefunSymsTySynD :: Name -> [DTyVarBndrVis] -> DType -> PrM [DDec]
 buildDefunSymsTySynD name tvbs rhs = defunReify name tvbs mb_res_kind
   where
     -- If a type synonym lacks a SAK, we can "infer" its result kind by
@@ -161,7 +162,7 @@ buildDefunSymsDataD ctors =
 -- and dsReifyType to determine whether defunctionalization should make use
 -- of SAKs or not (see Note [Defunctionalization game plan]).
 defunReify :: Name             -- Name of the declaration to be defunctionalized
-           -> [DTyVarBndrUnit] -- The declaration's type variable binders
+           -> [DTyVarBndrVis]  -- The declaration's type variable binders
                                -- (only used if the declaration lacks a SAK)
            -> Maybe DKind      -- The declaration's return kind, if it has one
                                -- (only used if the declaration lacks a SAK)
@@ -252,8 +253,8 @@ defunctionalize name m_fixity defun_ki = do
           -- gets to be that large.
           go :: Int -> [(Name, DKind)] -> [(Name, DKind)] -> (DKind, [DDec])
           go n arg_nks res_nkss =
-            let arg_tvbs :: [DTyVarBndrUnit]
-                arg_tvbs = map (\(na, ki) -> DKindedTV na () ki) arg_nks
+            let arg_tvbs :: [DTyVarBndrVis]
+                arg_tvbs = map (\(na, ki) -> DKindedTV na bndrReq ki) arg_nks
 
                 mk_sak_dec :: DKind -> DDec
                 mk_sak_dec res_ki =
@@ -262,7 +263,14 @@ defunctionalize name m_fixity defun_ki = do
             case res_nkss of
               [] ->
                 let sat_sak_dec = mk_sak_dec sak_res_ki
-                    sat_decs    = mk_sat_decs opts n arg_tvbs (Just sak_res_ki)
+
+#if __GLASGOW_HASKELL__ >= 909
+                    sak_tvbs' = mapMaybe kindVarToBndrVis sak_tvbs
+#else
+                    sak_tvbs' = []
+#endif
+
+                    sat_decs    = mk_sat_decs opts n (sak_tvbs' ++ arg_tvbs) (Just sak_res_ki)
                 in (sak_res_ki, sat_sak_dec:sat_decs)
               res_nk:res_nks ->
                 let (res_ki, decs)   = go (n+1) (arg_nks ++ [res_nk]) res_nks
@@ -281,7 +289,7 @@ defunctionalize name m_fixity defun_ki = do
     -- (see Note [Defunctionalization game plan], Wrinkle 1: Partial kinds)
     -- or a non-vanilla kind
     -- (see Note [Defunctionalization game plan], Wrinkle 2: Non-vanilla kinds).
-    defun_fallback :: [DTyVarBndrUnit] -> Maybe DKind -> PrM [DDec]
+    defun_fallback :: [DTyVarBndrVis] -> Maybe DKind -> PrM [DDec]
     defun_fallback tvbs' m_res' = do
       opts <- getOptions
       extra_name <- qNewName "arg"
@@ -310,7 +318,7 @@ defunctionalize name m_fixity defun_ki = do
           --   kinds are not always known. By a similar token, this function
           --   uses Maybe DKind, not DKind, as the type of @m_res_k@, since
           --   the result kind is not always fully known.
-          go :: Int -> [DTyVarBndrUnit] -> [DTyVarBndrUnit] -> (Maybe DKind, [DDec])
+          go :: Int -> [DTyVarBndrVis] -> [DTyVarBndrVis] -> (Maybe DKind, [DDec])
           go n arg_tvbs res_tvbss =
             case res_tvbss of
               [] ->
@@ -330,7 +338,7 @@ defunctionalize name m_fixity defun_ki = do
     mk_defun_decs :: Options
                   -> Int
                   -> Int
-                  -> [DTyVarBndrUnit]
+                  -> [DTyVarBndrVis]
                   -> Name
                   -> Name
                   -> Maybe DKind
@@ -340,7 +348,7 @@ defunctionalize name m_fixity defun_ki = do
           next_name   = defunctionalizedName opts name (n+1)
           con_name    = prefixName "" ":" $ suffixName "KindInference" "###" data_name
           arg_names   = map extractTvbName arg_tvbs
-          params      = arg_tvbs ++ [DPlainTV tyfun_name ()]
+          params      = arg_tvbs ++ [DPlainTV tyfun_name bndrReq]
           con_eq_ct   = DConT sameKindName `DAppT` lhs `DAppT` rhs
             where
               lhs = foldType (DConT data_name) (map DVarT arg_names) `apply` (DVarT extra_name)
@@ -356,7 +364,7 @@ defunctionalize name m_fixity defun_ki = do
                                   (DConT applyName `DAppT` app_data_ty
                                                    `DAppT` DVarT tyfun_name)
                                   (foldTypeTvbs (DConT app_eqn_rhs_name)
-                                                (arg_tvbs ++ [DPlainTV tyfun_name ()]))
+                                                (arg_tvbs ++ [DPlainTV tyfun_name bndrReq]))
           -- If the next defunctionalization symbol is fully saturated, then
           -- use the original declaration name instead.
           -- See Note [Fully saturated defunctionalization symbols]
@@ -379,15 +387,15 @@ defunctionalize name m_fixity defun_ki = do
     -- Generate a "fully saturated" defunction symbol, along with a fixity
     -- declaration (if needed).
     -- See Note [Fully saturated defunctionalization symbols].
-    mk_sat_decs :: Options -> Int -> [DTyVarBndrUnit] -> Maybe DKind -> [DDec]
+    mk_sat_decs :: Options -> Int -> [DTyVarBndrVis] -> Maybe DKind -> [DDec]
     mk_sat_decs opts n sat_tvbs m_sat_res =
       let sat_name = defunctionalizedName opts name n
           sat_dec  = DClosedTypeFamilyD
                        (DTypeFamilyHead sat_name sat_tvbs
                                         (maybeKindToResultSig m_sat_res) Nothing)
                        [DTySynEqn Nothing
-                                  (foldTypeTvbs (DConT sat_name) sat_tvbs)
-                                  (foldTypeTvbs (DConT name)     sat_tvbs)]
+                                  (foldTypeTvbsVis (DConT sat_name) sat_tvbs)
+                                  (foldTypeTvbsVis (DConT name)     sat_tvbs)]
           sat_fixity_dec = maybeToList $ fmap (mk_fix_decl sat_name) m_fixity
       in sat_dec : sat_fixity_dec
 
@@ -399,7 +407,7 @@ defunctionalize name m_fixity defun_ki = do
     --
     -- >>> eta_expand [(x :: a), (y :: b)] Nothing
     -- ([(x :: a), (y :: b)], Nothing)
-    eta_expand :: [DTyVarBndrUnit] -> Maybe DKind -> PrM ([DTyVarBndrUnit], Maybe DKind)
+    eta_expand :: [DTyVarBndrVis] -> Maybe DKind -> PrM ([DTyVarBndrVis], Maybe DKind)
     eta_expand m_arg_tvbs Nothing = pure (m_arg_tvbs, Nothing)
     eta_expand m_arg_tvbs (Just res_kind) = do
         let (arg_ks, result_k) = unravelDType res_kind
@@ -409,11 +417,11 @@ defunctionalize name m_fixity defun_ki = do
 
     -- Convert a DVisFunArg to a DTyVarBndr, generating a fresh type variable
     -- name if the DVisFunArg is an anonymous argument.
-    mk_extra_tvb :: DVisFunArg -> PrM DTyVarBndrUnit
+    mk_extra_tvb :: DVisFunArg -> PrM DTyVarBndrVis
     mk_extra_tvb vfa =
       case vfa of
-        DVisFADep tvb -> pure tvb
-        DVisFAAnon k  -> (\n -> DKindedTV n () k) <$>
+        DVisFADep tvb -> pure (bndrReq <$ tvb)
+        DVisFAAnon k  -> (\n -> DKindedTV n bndrReq k) <$>
                            -- Use noExactName below to avoid GHC#19743.
                            (noExactName <$> qNewName "e")
 
@@ -428,7 +436,7 @@ defunctionalize name m_fixity defun_ki = do
 -- information is used.
 data DefunKindInfo
   = DefunSAK DKind
-  | DefunNoSAK [DTyVarBndrUnit] (Maybe DKind)
+  | DefunNoSAK [DTyVarBndrVis] (Maybe DKind)
 
 -- Shorthand for building (k1 ~> k2)
 buildTyFunArrow :: DKind -> DKind -> DKind
diff --git a/src/Data/Singletons/TH/Single.hs b/src/Data/Singletons/TH/Single.hs
index 71fc1a9..bcd51f8 100644
--- a/src/Data/Singletons/TH/Single.hs
+++ b/src/Data/Singletons/TH/Single.hs
@@ -148,7 +148,7 @@ singDecideInstances = concatMapM singDecideInstance
 singDecideInstance :: OptionsMonad q => Name -> q [Dec]
 singDecideInstance name = do
   (tvbs, cons) <- getDataD ("I cannot make an instance of SDecide for it.") name
-  dtvbs <- mapM dsTvbUnit tvbs
+  dtvbs <- mapM dsTvbVis tvbs
   let data_ty = foldTypeTvbs (DConT name) dtvbs
   dcons <- concatMapM (dsCon dtvbs data_ty) cons
   let tyvars = map (DVarT . extractTvbName) dtvbs
@@ -201,7 +201,7 @@ singShowInstances = concatMapM singShowInstance
 showSingInstance :: OptionsMonad q => Name -> q [Dec]
 showSingInstance name = do
   (tvbs, cons) <- getDataD ("I cannot make an instance of Show for it.") name
-  dtvbs <- mapM dsTvbUnit tvbs
+  dtvbs <- mapM dsTvbVis tvbs
   let data_ty = foldTypeTvbs (DConT name) dtvbs
   dcons <- concatMapM (dsCon dtvbs data_ty) cons
   let tyvars    = map (DVarT . extractTvbName) dtvbs
@@ -273,7 +273,7 @@ singInstance :: OptionsMonad q => DerivDesc q -> String -> Name -> q [Dec]
 singInstance mk_inst inst_name name = do
   (tvbs, cons) <- getDataD ("I cannot make an instance of " ++ inst_name
                             ++ " for it.") name
-  dtvbs <- mapM dsTvbUnit tvbs
+  dtvbs <- mapM dsTvbVis tvbs
   let data_ty = foldTypeTvbs (DConT name) dtvbs
   dcons <- concatMapM (dsCon dtvbs data_ty) cons
   let data_decl = DataDecl name dtvbs dcons
diff --git a/src/Data/Singletons/TH/Single/Monad.hs b/src/Data/Singletons/TH/Single/Monad.hs
index febd859..8c3ed65 100644
--- a/src/Data/Singletons/TH/Single/Monad.hs
+++ b/src/Data/Singletons/TH/Single/Monad.hs
@@ -26,8 +26,10 @@ import Data.Singletons.TH.Promote.Monad ( emitDecs, emitDecsM )
 import Data.Singletons.TH.Util
 import Language.Haskell.TH.Syntax hiding ( lift )
 import Language.Haskell.TH.Desugar
-import Control.Monad.Reader
-import Control.Monad.Writer
+import Control.Monad ( liftM2 )
+import Control.Monad.IO.Class ( MonadIO )
+import Control.Monad.Reader ( MonadReader(..), ReaderT(..), asks )
+import Control.Monad.Writer ( MonadWriter, WriterT(..) )
 import Control.Applicative
 
 -- environment during singling
diff --git a/src/Data/Singletons/TH/Syntax.hs b/src/Data/Singletons/TH/Syntax.hs
index 024ba5a..af00fe3 100644
--- a/src/Data/Singletons/TH/Syntax.hs
+++ b/src/Data/Singletons/TH/Syntax.hs
@@ -46,10 +46,10 @@ instance Monoid PromDPatInfos where
 type SingDSigPaInfos = [(DExp, DType)]
 
 -- The parts of data declarations that are relevant to singletons-th.
-data DataDecl = DataDecl Name [DTyVarBndrUnit] [DCon]
+data DataDecl = DataDecl Name [DTyVarBndrVis] [DCon]
 
 -- The parts of type synonyms that are relevant to singletons-th.
-data TySynDecl = TySynDecl Name [DTyVarBndrUnit] DType
+data TySynDecl = TySynDecl Name [DTyVarBndrVis] DType
 
 -- The parts of open type families that are relevant to singletons-th.
 type OpenTypeFamilyDecl = TypeFamilyDecl 'Open
@@ -66,7 +66,7 @@ data FamilyInfo = Open | Closed
 data ClassDecl ann
   = ClassDecl { cd_cxt  :: DCxt
               , cd_name :: Name
-              , cd_tvbs :: [DTyVarBndrUnit]
+              , cd_tvbs :: [DTyVarBndrVis]
               , cd_fds  :: [FunDep]
               , cd_lde  :: LetDecEnv ann
               , cd_atfs :: [OpenTypeFamilyDecl]
diff --git a/src/Data/Singletons/TH/Util.hs b/src/Data/Singletons/TH/Util.hs
index ee7a50d..c7d4cdf 100644
--- a/src/Data/Singletons/TH/Util.hs
+++ b/src/Data/Singletons/TH/Util.hs
@@ -1,3 +1,4 @@
+{-# LANGUAGE CPP #-}
 {-# LANGUAGE LambdaCase #-}
 
 {- Data/Singletons/TH/Util.hs
@@ -16,10 +17,12 @@ import Language.Haskell.TH ( pprint )
 import Language.Haskell.TH.Syntax hiding ( lift )
 import Language.Haskell.TH.Desugar
 import Data.Char
-import Control.Monad hiding ( mapM )
-import Control.Monad.Except hiding ( mapM )
-import Control.Monad.Reader hiding ( mapM )
-import Control.Monad.Writer hiding ( mapM )
+import Control.Monad ( liftM, unless, when )
+import Control.Monad.Except ( ExceptT, runExceptT, MonadError(..) )
+import Control.Monad.IO.Class ( MonadIO )
+import Control.Monad.Reader ( MonadReader(..), Reader, ReaderT(..) )
+import Control.Monad.Trans ( MonadTrans )
+import Control.Monad.Writer ( MonadWriter(..), WriterT(..), execWriterT )
 import qualified Data.Map as Map
 import Data.Map ( Map )
 import Data.Bifunctor (second)
@@ -189,6 +192,22 @@ extractTvbName (DKindedTV n _ _) = n
 tvbToType :: DTyVarBndr flag -> DType
 tvbToType = DVarT . extractTvbName
 
+extractTvbFlag :: DTyVarBndr flag -> flag
+extractTvbFlag (DPlainTV _ f) = f
+extractTvbFlag (DKindedTV _ f _) = f
+
+#if __GLASGOW_HASKELL__ >= 909
+appTvbToType :: DType -> DTyVarBndrVis -> DType
+appTvbToType ty tvb
+  | BndrInvis <- extractTvbFlag tvb = DAppKindT ty (tvbToType tvb)
+  | otherwise = DAppT ty (tvbToType tvb)
+
+kindVarToBndrVis :: DTyVarBndrSpec -> Maybe DTyVarBndrVis
+kindVarToBndrVis tvb
+  | SpecifiedSpec <- extractTvbFlag tvb = Just (BndrInvis <$ tvb)
+  | otherwise = Nothing
+#endif
+
 -- If a type variable binder lacks an explicit kind, pick a default kind of
 -- Type. Otherwise, leave the binder alone.
 defaultTvbToTypeKind :: DTyVarBndr flag -> DTyVarBndr flag
@@ -362,9 +381,9 @@ filterInvisTvbArgs (DFAForalls tele args) =
     DForallInvis tvbs' -> tvbs' ++ res
 
 -- Infer the kind of a DTyVarBndr by using information from a DVisFunArg.
-replaceTvbKind :: DVisFunArg -> DTyVarBndrUnit -> DTyVarBndrUnit
-replaceTvbKind (DVisFADep tvb) _   = tvb
-replaceTvbKind (DVisFAAnon k)  tvb = DKindedTV (extractTvbName tvb) () k
+replaceTvbKind :: DVisFunArg -> DTyVarBndrVis -> DTyVarBndrVis
+replaceTvbKind (DVisFADep tvb) _   = bndrReq <$ tvb
+replaceTvbKind (DVisFAAnon k)  tvb = DKindedTV (extractTvbName tvb) bndrReq k
 
 -- changes all TyVars not to be NameU's. Workaround for GHC#11812/#17537/#19743
 noExactTyVars :: Data a => a -> a
@@ -440,12 +459,21 @@ foldType = foldl DAppT
 foldTypeTvbs :: DType -> [DTyVarBndr flag] -> DType
 foldTypeTvbs ty = foldType ty . map tvbToType
 
+#if __GLASGOW_HASKELL__ >= 909
+-- apply a type to a list of type variable binders with accordance to its visibility
+foldTypeTvbsVis :: DType -> [DTyVarBndrVis] -> DType
+foldTypeTvbsVis ty = foldl appTvbToType ty
+#else
+foldTypeTvbsVis :: DType -> [DTyVarBndr flag] -> DType
+foldTypeTvbsVis = foldTypeTvbs
+#endif
+
 -- Construct a data type's variable binders, possibly using fresh variables
 -- from the data type's kind signature.
-buildDataDTvbs :: DsMonad q => [DTyVarBndrUnit] -> Maybe DKind -> q [DTyVarBndrUnit]
+buildDataDTvbs :: DsMonad q => [DTyVarBndrVis] -> Maybe DKind -> q [DTyVarBndrVis]
 buildDataDTvbs tvbs mk = do
   extra_tvbs <- mkExtraDKindBinders $ fromMaybe (DConT typeKindName) mk
-  pure $ tvbs ++ extra_tvbs
+  pure $ tvbs ++ changeDTVFlags bndrReq extra_tvbs
 
 -- apply an expression to a list of expressions
 foldExp :: DExp -> [DExp] -> DExp
